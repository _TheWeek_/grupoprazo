﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Map
{
    public sealed class PostMap : EntityTypeConfiguration<Post>
    {
        public PostMap()
        {
            ToTable("post");

            HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasColumnName("id")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Title)
                .HasColumnName("title")
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Created)
                .HasColumnName("created")
                .IsRequired();

            HasMany(c => c.Tags)
                .WithMany(c => c.Posts)
                .Map(c =>
                {
                    c.ToTable("poststags");
                    c.MapLeftKey("postid");
                    c.MapRightKey("tagid");
                });


        }
    }
}