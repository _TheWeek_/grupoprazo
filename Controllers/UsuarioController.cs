﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Models;
using GrupoPrazo.DAL;
using System.Data.Entity;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using SautinSoft;
using System.Text.RegularExpressions;

namespace GrupoPrazo.Controllers
{

    public class UsuarioController : Controller
    {
        private EFContext db = new EFContext();


        public ActionResult Index()
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var vaUsu = new Usuario();
            return View(vaUsu);

        }

        [HttpPost]
        public ActionResult Cadastrar([Bind(Include = "Id,Nome,Email,Permissao,Senha")] Usuario usuario)
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            if (usuario.Id == 0)
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();
            }
            else
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
            }

            var vaListaUsua = db.Usuarios.ToList();
            var vaListaUsuario = new List<Usuario>();

            vaListaUsuario = ConvertToPermissao(vaListaUsua);

            List<Tag> tag = new List<Tag>();
            Tag t = new Tag();
            t.Description = "teste 1";
            tag.Add(t);
            t = new Tag();
            t.Description = "teste 2";
            tag.Add(t);

            List<Post> post = new List<Post>();
            Post p = new Post();
            p.Title = "teste 1";
            p.Created = new DateTime(2018, 5, 1);
            p.Tags = tag;
            post.Add(p);
            p = new Post();
            p.Title = "teste 2";
            p.Created = new DateTime(2018, 5, 1);
            p.Tags = tag;
            post.Add(p);

            var vaLista = db.Set<Tag>().ToList();

            db.Post.AddRange(post);
            db.SaveChanges();

            return View("Listar", vaListaUsuario);
        }


        public ActionResult Listar()
        {
            /*if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }*/

            var vaListaUsua = db.Usuarios.ToList();
            var vaListaUsuario = new List<Usuario>();
            string pdfData = Convert.ToBase64String(System.IO.File.ReadAllBytes(@"C:\Users\willa\Documents\willian_da_silva_dantas_Curriculo.pdf"));
            byte[] pdfContent = System.IO.File.ReadAllBytes(@"C:\Users\willa\Documents\willian_da_silva_dantas_Curriculo.pdf");

            int pageCount;
            MemoryStream stream = new MemoryStream(pdfContent);
            using (var r = new StreamReader(stream))
            {
                string pdfText = r.ReadToEnd();
                Regex regx = new Regex(@"/Type\s*/Page[^s]");
                MatchCollection matches = regx.Matches(pdfText);
                pageCount = matches.Count;
            }

            vaListaUsuario = ConvertToPermissao(vaListaUsua, pdfData);
            TesteUpload();
            

           
            return View("Listar", vaListaUsuario);
        }

        [HttpGet]
        public ActionResult Alterar(int paId)
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var vaUsua = db.Usuarios.Where(a => a.Id == paId).FirstOrDefault();
            return View("Index", vaUsua);
        }


        [HttpGet]
        public ActionResult Deletar(int paId)
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var vaUsua = db.Usuarios.Where(a => a.Id == paId).FirstOrDefault();
            if (vaUsua != null)
            {
                db.Usuarios.Remove(vaUsua);
                db.SaveChanges();
            }

            var vaListaUsua = db.Usuarios.ToList();
            var vaListaUsuario = new List<Usuario>();

            vaListaUsuario = ConvertToPermissao(vaListaUsua);

            return View("Listar", vaListaUsuario);


        }



        public ActionResult Sair()
        {
            Session["usuarioLogadoID"] = null;
            Session["nomeUsuarioLogado"] = null;
            Session["Permissao"] = null;
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Pdf()
        {
           
            byte[] pdfData = System.IO.File.ReadAllBytes(@"C:\Users\willa\Documents\willian_da_silva_dantas_Curriculo.pdf");
            /*var arquivo = File(pdfData.ToArray(), //The binary data of the XLS file
                "application/pdf", //MIME type of Excel files
                string.Format("Teste_{0}.xlsx", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")));*/
            var arquivo = Convert.ToBase64String(pdfData);
            return Json(arquivo, JsonRequestBehavior.AllowGet);

            /* PdfFocus f = new PdfFocus();
             f.OpenPdf(@"C:\Users\willa\Documents\willian_da_silva_dantas_Curriculo.pdf");
             string result = null;
             if (f.PageCount > 0)
             {
                 result = f.ToHtml();
                 int resultado = f.ToHtml(@"C:\Users\willa\Documents\willian_da_silva_dantas_Curriculo.html");

             }

             return Json(result, JsonRequestBehavior.AllowGet);*/
        }

        public ActionResult Arquivo()
        {

            XSSFWorkbook hssfwb;
            string path = @"C:\Users\willa\Documents\Visual Studio 2015\Projects\TesteGrupoPrazo\GrupoPrazo\Template\Template.xls";
            using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                hssfwb = new XSSFWorkbook(file);
                file.Close();
            }

            ISheet sheet = hssfwb.GetSheetAt(0);
            IRow row = sheet.GetRow(0);

            int rowNumer = 0;

            //---- HEADER


            ICell cell;

            ICellStyle style = hssfwb.CreateCellStyle();

            cell = row.CreateCell(0);
            cell.SetCellValue("Nome");
            cell.CellStyle = style;

            cell = row.CreateCell(1);
            cell.SetCellValue("Telefone");
            cell.CellStyle = style;

            cell = row.CreateCell(2);
            cell.SetCellValue("X");
            cell.CellStyle = style;

            cell = row.CreateCell(3);
            cell.SetCellValue("Y");
            cell.CellStyle = style;

            cell = row.CreateCell(4);
            cell.SetCellValue("Soma");
            cell.CellStyle = style;

            //---- row
            rowNumer++;
            row = sheet.CreateRow(rowNumer);
            row.CreateCell(0).SetCellValue("Eduardo");
            row.CreateCell(1).SetCellValue("111111");
            row.CreateCell(2).SetCellValue("10");
            row.CreateCell(3).SetCellValue("7");
            row.CreateCell(4).SetCellFormula("C2+D2");

            //---- row
            rowNumer++;
            row = sheet.CreateRow(rowNumer);
            row.CreateCell(0).SetCellValue("Coutinho");
            row.CreateCell(1).SetCellValue("222222");
            row.CreateCell(2).SetCellValue("1");
            row.CreateCell(3).SetCellValue("2");
            row.CreateCell(4).SetCellFormula("C3+D3");

            //Resolve problema: O Excel encontrou conteúdo ilegível / Invalid or corrupt file (unreadable content)
            while (rowNumer > 20)
            {
                rowNumer++;
                row = sheet.CreateRow(rowNumer);
                row.CreateCell(0).SetCellValue(" ");
                row.CreateCell(1).SetCellValue(" ");
            }

            //Tamanho das colunas
            sheet.SetColumnWidth(0, 40 * 256);
            sheet.SetColumnWidth(1, 20 * 256);

            MemoryStream stream = new MemoryStream();
            hssfwb.Write(stream);

            var arquivo = File(stream.ToArray(), //The binary data of the XLS file
                "application/vnd.ms-excel", //MIME type of Excel files
                string.Format("Teste_{0}.xlsx", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"))); //Suggested file name in the "Save as" dialog which will be displayed to the end user 

            return Json(arquivo, JsonRequestBehavior.AllowGet);
        }


        public List<Usuario> ConvertToPermissao(List<Usuario> paListUsuario, string data)
        {
            var vaListaUsuario = new List<Usuario>();
            foreach (Usuario vaI in paListUsuario)
            {
                var vaUsuario = new Usuario();
                vaUsuario.Id = vaI.Id;
                vaUsuario.Nome = vaI.Nome;
                vaUsuario.Email = vaI.Email;
                vaUsuario.Senha = vaI.Senha;
                vaUsuario.pdf = data;
                if (vaI.Permissao == "A")
                {
                    vaUsuario.Permissao = "Adiministrador";
                }
                else
                {
                    vaUsuario.Permissao = "Usuário Basico";
                }

                vaListaUsuario.Add(vaUsuario);
            }

            return vaListaUsuario;
        }


        public List<Usuario> ConvertToPermissao(List<Usuario> paListUsuario)
        {
            var vaListaUsuario = new List<Usuario>();
            foreach (Usuario vaI in paListUsuario)
            {
                var vaUsuario = new Usuario();
                vaUsuario.Id = vaI.Id;
                vaUsuario.Nome = vaI.Nome;
                vaUsuario.Email = vaI.Email;
                vaUsuario.Senha = vaI.Senha;

                if (vaI.Permissao == "A")
                {
                    vaUsuario.Permissao = "Adiministrador";
                }
                else
                {
                    vaUsuario.Permissao = "Usuário Basico";
                }

                vaListaUsuario.Add(vaUsuario);
            }

            return vaListaUsuario;
        }


        public void TesteUpload()
        {
            string path = @"C:\Users\willa\Documents\willian_da_silva_dantas_Curriculo.pdf";
            string caminho = AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_Data\\Anexos\\tb_cid.pdf";

            Stream p = System.IO.File.OpenRead(path);
            using (MemoryStream ms = new MemoryStream())
            {
                p.CopyTo(ms);
                var original = ms.ToArray();
                System.IO.File.WriteAllBytes(caminho, original);
            }



        }


    }
}