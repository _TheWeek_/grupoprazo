﻿using GrupoPrazo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;
using System.Data.Entity;

namespace GrupoPrazo.Controllers
{
    public class TarefaController : Controller
    {

        private EFContext db = new EFContext();


        public ActionResult Index()
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var vaTarefa = new Tarefa();
            return View(vaTarefa);
        }



        public ActionResult Listar()
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var vaTarefa = db.Tarefas.ToList();
            return View("Listar", vaTarefa);
        }


        [HttpPost]
        public ActionResult Cadastrar([Bind(Include = "id,Name")] Tarefa tarefa)
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            if (tarefa.id == 0)
            {
                db.Tarefas.Add(tarefa);
                db.SaveChanges();
            }
            else
            {
                db.Entry(tarefa).State = EntityState.Modified;
                db.SaveChanges();
            }


            var vaListaTarefa = db.Tarefas.ToList();

            return View("Listar", vaListaTarefa);
        }


        [HttpGet]
        public ActionResult Alterar(int paId)
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var vaTarefa = db.Tarefas.Where(a => a.id == paId).FirstOrDefault();
            return View("Index", vaTarefa);
        }


        [HttpGet]
        public ActionResult Deletar(int paId)
        {
            if (Session["usuarioLogadoID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var vaTarefa = db.Tarefas.Where(a => a.id == paId).FirstOrDefault();
            if (vaTarefa != null)
            {
                db.Tarefas.Remove(vaTarefa);
                db.SaveChanges();
            }

            var vaListaTarefa = db.Tarefas.ToList();

            return View("Listar", vaListaTarefa);


        }

    }
}