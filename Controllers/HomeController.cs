﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;

namespace GrupoPrazo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Index
        public ActionResult  Index()
        {
            var vaUsu = new Usuario();

            if (Session["usuarioLogadoID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

           

        }


        
    }
}