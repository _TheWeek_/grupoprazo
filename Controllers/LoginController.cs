﻿using GrupoPrazo.DAL;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{
    public class LoginController : Controller
    {
        private EFContext db = new EFContext();
        // GET: GrupoPrazo
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Autenticar(string usuario, string senha)
        {
            var vaUsu = new Usuario();
            vaUsu.Nome = usuario;
            vaUsu.Senha = senha;

           
            var usu = db.Usuarios.Where(a => a.Nome.Equals(usuario) && a.Senha.Equals(senha)).FirstOrDefault();
            var usuarios = db.Usuarios.Where(x => x.Nome == usuario && x.Senha == senha).FirstOrDefault();

            if(usu != null)
            {
                Session["usuarioLogadoID"] = usu.Id.ToString();
                Session["nomeUsuarioLogado"] = usu.Nome.ToString();
                Session["Permissao"] = usu.Permissao; 
                return RedirectToAction("Index", "Home");
            }

            return View(vaUsu);

        }



    }
}