﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Post
    {
        
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public virtual List<Tag> Tags { get; set; }
    }
}