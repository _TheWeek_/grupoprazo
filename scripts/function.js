﻿/// <reference path="pdf.js/build/pdf.worker.js" />
/// <reference path="pdf.js/build/pdf.worker.js" />
function action(controller, action) {
    window.location.href = controller + '/' + action;
}


function alterar(id) {
    window.location.href = '../Usuario/Alterar?paId=' + id;
}


function excluir(id) {
    window.location.href = '../Usuario/Deletar?paId=' + id;
}


function tarefaAlterar(id) {
    window.location.href = '../Tarefa/Alterar?paId=' + id;
}


function tarefaExcluir(id) {
    window.location.href = '../Tarefa/Deletar?paId=' + id;
}


function newPopup(url) {
    popupWindow = window.open(url, 'popUpWindow', 'height=300,width=500,left=200,top=200,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}



$(document).ready(function () {
    $("#salvar").click(function () {
        $('#meuModal').modal('show');
        modalConfirm(function (confirm) {
            if (confirm) {
                $(".form-principal").submit()
            } else {
                return false;
            }
        });
    });

 

    $('input[type=text], select, textarea').on('blur', function () {
        if (($(this).attr('required') == 'required') && ($(this).val() == '' || $(this).val() == null || $(this).val() == '...')) {
            removeErrorLabel($(this));
            addErrorLabel($(this), '* Campo Obrigatorio');
        } else {
            removeErrorLabel($(this));
        }
    });



    $('.color').change(function () {
        var element = $(this)
        if (element.is(":checked")) {
            var vaX = element.parents('tr').children('td').eq(0);
            $(vaX).children('font')[0].style.color = "#00008B";
            $(vaX).children('font')[0].style.textDecoration = 'underline';
        } else {
            var vaX = element.parents('tr').children('td').eq(0);
            $(vaX).children('font')[0].style.color = "#666";
            $(vaX).children('font')[0].style.textDecoration = 'none';
        }
        
    });


    $("#arquivo").click(function () {

        $.ajax({
            type: "GET",
            url: "/Usuario/Arquivo/",
            contentType: "application/json; charset=utf-8",
            data: null,
            success: function (Rdata) {
                debugger;
                var bytes = new Uint8Array(Rdata.FileContents);
                var blob = new Blob([bytes], { type: 'application/vnd.ms-excel' });
                var downloadUrl = URL.createObjectURL(blob);
                var a = document.createElement("a");
                a.href = downloadUrl;
                a.download = "downloadFile.xlsx";
                document.body.appendChild(a);
                a.click();

            },
            error: function (err) {

            }

        });

    });


   


    $("#pdf").click(function () {

        $.ajax({
            type: "GET",
            url: "/Usuario/Pdf/",
            contentType: "application/json; charset=utf-8",
            data: null,
            success: function (Rdata) {
                debugger;


                var pdfData = atob(Rdata);

                var pdfjsLib = window['pdfjs-dist/build/pdf'];

                // The workerSrc property shall be specified.
                pdfjsLib.GlobalWorkerOptions.workerSrc = "/scripts/pdf.js/build/pdf.worker.js";

                // Using DocumentInitParameters object to load binary data.
                var loadingTask = pdfjsLib.getDocument({ data: pdfData });
                loadingTask.promise.then(function (pdf) {
                    console.log('PDF loaded');

                    // Fetch the first page
                    var pageNumber = pdf.numPages;
                    for (var i = 1; i <= pageNumber; i++) {
                        pdf.getPage(i).then(function (page) {
                            console.log('Page loaded');

                            var scale = 1.7;
                            var viewport = page.getViewport(scale);

                            // Prepare canvas using PDF page dimensions
                            //var canvas = document.getElementById('the-canvas');
                            var canvas = document.createElement("canvas");
                            var context = canvas.getContext('2d');
                            canvas.height = viewport.height;
                            canvas.width = viewport.width;
                            $("#page-wrapper").append(canvas);
                            // Render PDF page into canvas context
                            var renderContext = {
                                canvasContext: context,
                                viewport: viewport
                            };
                            var renderTask = page.render(renderContext);
                            renderTask.promise.then(function () {
                                console.log('Page rendered');
                            });
                        });
                    }

                }, function (reason) {
                    // PDF loading error
                    console.error(reason);
                });


                /* var bytes = new Uint8Array(Rdata.FileContents);
                var blob = new Blob([bytes], { type: 'application/pdf' });
                var downloadUrl = URL.createObjectURL(blob);
                var iframe = document.createElement("iframe");
 
              
                iframe.src = downloadUrl + "#toolbar=0";
                iframe.width = "100%";
                iframe.height = "400px";
                iframe.id = "framePdf";

                

             
                $("#page-wrapper").append(iframe);

                newPopup(downloadUrl);
               
                
                var bytes = new Uint8Array(Rdata.FileContents);
                var blob = new Blob([bytes], { type: 'application/vnd.pdf' });
                var downloadUrl = URL.createObjectURL(blob);
                var a = document.createElement("a");
                a.href = downloadUrl;
                a.download = "downloadFile.pdf";
                document.body.appendChild(a);
                a.click();
                */
            },
            error: function (err) {
                alert("erro");
            }
        });

    });
 
   

});



var modalConfirm = function (callback) {

    $("#modal-btn-si").on("click", function () {
        callback(true);
        $("#meuModal").modal('hide');
    });

    $("#modal-btn-no").on("click", function () {
        callback(false);
        $("#meuModal").modal('hide');
    });
};




function addErrorLabel(local, textMsg) {
    var label = document.createElement('label');
    $(label).addClass('control-label').addClass('errorLabel').text(textMsg);
    $(local).parent().append(label);
    $(local).parent().addClass('has-error');
    label = '';
}

function removeErrorLabel(local) {
    $(local).parent().removeClass('has-error');
    $(local).parent().find('.errorLabel').remove();
}

